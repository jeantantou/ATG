# Todo

- Automatiser les `\begin{enumerate}[leftmargin=* ,parsep=0cm,itemsep=0cm,topsep=0cm]`
- Automatiser les `\noident` **FAIT**
- Rendre la structure plus claire
- Supprimer les `\noindent\ref{Quot}.2 \textbf{Lemme.}` et utiliser des environnements `lemme`
- Remettre en forme les sections et les sous sections vides
- Utiliser des `\mapsto` là où ils sont nécessaires
- Coriger les erreurs de typographie
- Corriger les coquilles mathématiques
- Indenter le code
- Éventuellement supprimer les `\textbf` au profit d'environnements, mais faire attention à la notation
- Ajouter les diagrammes
- Terminer la partie III
